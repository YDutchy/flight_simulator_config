import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    /**
     * This program should not need any arguments.
     * It takes the file from the current directory, modifies it and stores it as scenery_new.cfg.
     * After this conversion the user will have to manually change it.
     * @param args Basic setup.
     */
    public static void main(String[] args) {
        // Simple message to, hopefully, help anyone that is in need of problems.
        System.out.println("Starting to optimise your file. Make sure it is named scenery.cfg :)");

        // Get the absolute path of the current working directory.
        String absolutePath = System.getProperty("user.dir");

        // Call the method which will update the scenery file with the absolute path of the to be edited file.
        makeFile(absolutePath);
    }

    /**
     * This method will open the file and create a new file accordingly.
     * The new file will be scenery_new.cfg.
     * @param absolutePath The path to the file which will be updated.
     */
    static private void makeFile(String absolutePath) {
        // A list that keeps all the lines that are read until printed.
        List<String> lineList = new ArrayList<>();

        // A String that hold the current line to be verified.
        String currentLine;

        // An integer that keeps track of the current Area/ Layer number.
        int i = 1;

        try {
            // Create a file that is according to the absolute path + the name of the file (Always scenery.cfg).
            File file = new File(absolutePath + "\\scenery.cfg");

            // Create a new file that is according to the absolute path + the name of the file (Always scenery_new.cfg).
            File newFile = new File(absolutePath + "\\scenery_new.cfg");

            // Set up a bufferedReader which will handle processing the file.
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            // Read through the whole file and replace the data as needed.
            while ((currentLine = bufferedReader.readLine()) != null) {
                if (currentLine.contains("[Area.")) {
                    currentLine = currentLine.replace(currentLine, "[Area." + i + "]");
                }
                if (currentLine.contains("Layer=")) {
                    currentLine = currentLine.replace(currentLine, "Layer=" + i);
                    i++;
                }
                lineList.add(currentLine + "\n");
            }

            // Close the reader as it is not needed anymore.
            bufferedReader.close();

            // Create a write to make the new file.
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFile));

            // Write all the data into the new file.
            for (String s: lineList) {
                bufferedWriter.write(s);
            }

            // Flush and close the writer.
            bufferedWriter.flush();
            bufferedWriter.close();

            System.out.println("File has successfully been created and stored :)");
        } catch (IOException e) {
            System.out.println("Error with your file" + e.getMessage());
        }
    }
}
